package financeiro.util;

public class UtilException extends Exception {
	private static final long serialVersionUID = -8615765549184290334L;

	public UtilException() {
		// TODO Auto-generated constructor stub
	}

	public UtilException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UtilException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UtilException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public UtilException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
