package financeiro.usuario;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class UsuarioDAOHibernate implements UsuarioDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Usuario usuario) {
		session.save(usuario);
	}

	@Override
	public void atualizar(Usuario usuario) {
		if (usuario.getPermissao() == null || usuario.getPermissao().size() == 0) {
			Usuario usuarioPermissao = this.carregar(usuario.getCodigo());
			usuario.setPermissao(usuarioPermissao.getPermissao());
			this.session.evict(usuarioPermissao);
		}
		this.session.update(usuario);
	}

	@Override
	public void excluir(Usuario usuario) {
		session.delete(usuario);
	}

	/**
	 * Realizamos um SELECT no banco de dados para carregar todos os dados de
	 * usuario recebidos no par�metro.
	 */
	@Override
	public Usuario carregar(Integer codigo) {
		return (Usuario) this.session.get(Usuario.class, codigo);
	}

	/**
	 * Consultamos todos os usuarios do banco de dados, devolvendo um
	 * java.util.List<Usuario>. Opera��o realizada para exibir uma listagem dos
	 * usuarios cadastrados na area administrativa do sistema.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listar() {
		return this.session.createCriteria(Usuario.class).list();
		// For�a uma consulta ao BD sem nenhuma condi��o.
	}

	/** Realizamos uma consulta ao banco de dados utilizando uma query.
	 * 
	 */
	@Override
	public Usuario buscarPorLogin(String login) {
		String hql = "SELECT u FROM Usuario u WHERE u.login = :login";
		Query consulta = this.session.createQuery(hql);
		consulta.setString("login", login);
		return (Usuario) consulta.uniqueResult();
	}

}
